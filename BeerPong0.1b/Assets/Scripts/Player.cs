﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public GameObject ball;
    public GameObject playerCamera;
    public GameObject malcom;
    public GameObject ballCount;
    public GameObject playerTwo;
    public Camera playerTwoCamera;
    public Camera playerOneCamera;

    public float resetTimer = 5f;
    public float ballDistance = 2f;
    public float ballThrowingForce = 5f;
    public bool holdingBall = true;

	// Use this for initialization
	void Start () {
        ball.GetComponent<Rigidbody>().useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
        playerCamera.transform.position = malcom.transform.position + malcom.transform.up * 2 + malcom.transform.forward * 0.2f;
        if (holdingBall)
        {
            ball.transform.position = playerCamera.transform.position + playerCamera.transform.forward * 1;

            if (Input.GetMouseButtonDown(0))
            {
                holdingBall = false;
                ball.GetComponent<Rigidbody>().useGravity = true;
                ball.GetComponent<Rigidbody>().AddForce(playerCamera.transform.forward * ballThrowingForce);    //propel the ball
                int currentBalls = int.Parse(ballCount.GetComponent<Text>().text) - 1; //subtract 1 to the score
                ballCount.GetComponent<Text>().text = currentBalls.ToString();
            }
            
        }
        if (!holdingBall)
        {
            resetTimer -= Time.deltaTime;
            if (resetTimer <= 0)
            {
                ball.GetComponent<Rigidbody>().transform.position = playerCamera.transform.position + playerCamera.transform.forward * 1;
                holdingBall = true;
                resetTimer = 5;
                ball.GetComponent<Rigidbody>().useGravity = false;
                
            }
        }
    }
}
