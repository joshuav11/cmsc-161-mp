﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TwoPlayerController : MonoBehaviour {
    public GameObject playerOne;
    public Camera playerOneCamera;
    public GameObject playerTwo;
    public Camera playerTwoCamera;

    public float resetTimer = 5f;
    public bool holdingBall = true;

    // Use this for initialization
    private void Start()
    {
        playerOneCamera.enabled = true;
        playerTwoCamera.enabled = false;
        playerOne.SetActive(true);
        playerTwo.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            holdingBall = false;
        }
        if (!holdingBall)
        {
            resetTimer -= Time.deltaTime;
            if (resetTimer <= 0)
            {
                playerOneCamera.enabled = !playerOneCamera.enabled;
                playerTwoCamera.enabled = !playerTwoCamera.enabled;
                holdingBall = true;
                resetTimer = 5;
                if (!playerTwo.activeInHierarchy)
                {
                    playerTwo.SetActive(true);
                    playerOne.SetActive(false);
                }
                else
                {
                    playerOne.SetActive(true);
                    playerTwo.SetActive(false);
                }
            }
        }
    }
}
