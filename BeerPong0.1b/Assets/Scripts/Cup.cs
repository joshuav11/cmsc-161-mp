﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cup : MonoBehaviour
{

    public GameObject score; //reference to the ScoreText gameobject, set in editor
    public GameObject cup; //reference to the basket sound
    public GameObject pongcup;



    void OnCollisionEnter() //if ball hits board
    {
        // audio.Play(); //plays the hit board sound
        cup.GetComponent<AudioSource>().Play();
    }

    void OnTriggerEnter() //if ball hits basket collider
    {
        int currentScore = int.Parse(score.GetComponent<Text>().text) + 1; //add 1 to the score
        score.GetComponent<Text>().text = currentScore.ToString();
        //AudioSource.PlayClipAtPoint(cup.GetComponent<AudioClip>(), transform.position); //play basket sound

        Destroy(pongcup);
    }
}
